# Makefile for top level of lmbench
# $Id: Makefile 1.17 00/05/31 16:16:15+03:00 staelin@hpli8.hpli.hpl.hp.com $

# Possible things to $(MAKE):
#
# build		(default) go to the source directory and build the benchmark
# results	go to the source directory and build and run the benchmark
# rerun		run the benchmark again
# see		see the results that came with this release
#		Go to the results directory and read the Makefile.
# doc.lpr	print the documentation
# doc.x		preview the documentation (needs X, groff, pic, etc)
# clean		go to the subdirs and $(MAKE) clean
# get		$(MAKE) sure all files are checked out
# shar		build a shippable shar archive

SHELL=/bin/sh
TARGET=riscv64-unknown-linux-gnu

libtirpc/install/lib/libtirpc.a libtirpc/install/include/tirpc/rpc/rpc.h: libtirpc/Makefile libtirpc/install
	+make -C libtirpc install

libtirpc/install:
	mkdir libtirpc/install

libtirpc/configure: libtirpc/configure.ac
	cd libtirpc && autoupdate && ./bootstrap

libtirpc/Makefile: libtirpc/configure
	cd libtirpc && ./configure --host=$(TARGET) --disable-gssapi --disable-shared --prefix=`pwd`/install

build: libtirpc/install/lib/libtirpc.a libtirpc/install/include/tirpc/rpc/rpc.h
	cd src && $(MAKE) OS=$(TARGET) CC=$(TARGET)-gcc TIRPC_CFLAGS="-I`pwd`/../libtirpc/install/include/tirpc" TIRPC_LDFLAGS="-L`pwd`/../libtirpc/install/lib -ltirpc"

results: FRC
	cd src && $(MAKE) results

rerun: 
	cd src && $(MAKE) rerun

see:
	cd results && $(MAKE) summary percent 2>/dev/null | more

doc.lpr:
	cd doc && $(MAKE) PS && lpr *.PS

doc.x:
	cd doc && $(MAKE) x

clobber clean: 
	for i in doc src results scripts; do \
		echo ===== $$i =====; \
		(cd $$i && $(MAKE) clean); \
	done
	/bin/rm -rf bin/*
	-bk clean 

get: 
	for i in doc src results scripts; do \
		echo ===== $$i =====; \
		(cd $$i && bk get -q); \
	done
	@co -q

info: 
	for i in doc src results scripts; do \
		echo ===== $$i =====; \
		(cd $$i && info); \
	done

release: scripts/mkrelease
	scripts/mkrelease

scripts/mkrelease:
	cd scripts && co mkrelease

# XXX - . must be named lmbench for this to work
shar:
	$(MAKE) clean
	co -q Makefile
	$(MAKE) get
	cd .. && \
	find lmbench -type f -print  | egrep -v 'noship|RCS' > /tmp/FILES
	cd .. && shar -S -a -n lmbench1.0 -L 50K < /tmp/FILES 

FRC:
